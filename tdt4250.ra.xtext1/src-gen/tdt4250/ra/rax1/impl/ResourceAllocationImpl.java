/**
 * generated by Xtext 2.18.0.M3
 */
package tdt4250.ra.rax1.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import tdt4250.ra.rax1.Person;
import tdt4250.ra.rax1.Rax1Package;
import tdt4250.ra.rax1.ResourceAllocation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Allocation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.ra.rax1.impl.ResourceAllocationImpl#getPerson <em>Person</em>}</li>
 *   <li>{@link tdt4250.ra.rax1.impl.ResourceAllocationImpl#getPercentage <em>Percentage</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceAllocationImpl extends MinimalEObjectImpl.Container implements ResourceAllocation
{
  /**
   * The cached value of the '{@link #getPerson() <em>Person</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPerson()
   * @generated
   * @ordered
   */
  protected Person person;

  /**
   * The default value of the '{@link #getPercentage() <em>Percentage</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPercentage()
   * @generated
   * @ordered
   */
  protected static final int PERCENTAGE_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getPercentage() <em>Percentage</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPercentage()
   * @generated
   * @ordered
   */
  protected int percentage = PERCENTAGE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ResourceAllocationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Rax1Package.Literals.RESOURCE_ALLOCATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Person getPerson()
  {
    if (person != null && person.eIsProxy())
    {
      InternalEObject oldPerson = (InternalEObject)person;
      person = (Person)eResolveProxy(oldPerson);
      if (person != oldPerson)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, Rax1Package.RESOURCE_ALLOCATION__PERSON, oldPerson, person));
      }
    }
    return person;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetPerson()
  {
    return person;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setPerson(Person newPerson)
  {
    Person oldPerson = person;
    person = newPerson;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Rax1Package.RESOURCE_ALLOCATION__PERSON, oldPerson, person));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int getPercentage()
  {
    return percentage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setPercentage(int newPercentage)
  {
    int oldPercentage = percentage;
    percentage = newPercentage;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Rax1Package.RESOURCE_ALLOCATION__PERCENTAGE, oldPercentage, percentage));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Rax1Package.RESOURCE_ALLOCATION__PERSON:
        if (resolve) return getPerson();
        return basicGetPerson();
      case Rax1Package.RESOURCE_ALLOCATION__PERCENTAGE:
        return getPercentage();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Rax1Package.RESOURCE_ALLOCATION__PERSON:
        setPerson((Person)newValue);
        return;
      case Rax1Package.RESOURCE_ALLOCATION__PERCENTAGE:
        setPercentage((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Rax1Package.RESOURCE_ALLOCATION__PERSON:
        setPerson((Person)null);
        return;
      case Rax1Package.RESOURCE_ALLOCATION__PERCENTAGE:
        setPercentage(PERCENTAGE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Rax1Package.RESOURCE_ALLOCATION__PERSON:
        return person != null;
      case Rax1Package.RESOURCE_ALLOCATION__PERCENTAGE:
        return percentage != PERCENTAGE_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (percentage: ");
    result.append(percentage);
    result.append(')');
    return result.toString();
  }

} //ResourceAllocationImpl
