package tdt4250.dict1.no;

public interface Words extends Iterable<CharSequence> {
	public boolean hasWord(CharSequence word);
}
