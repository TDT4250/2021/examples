package tdt4250.dict3.util;

public interface MutableWords extends Words {
	public void addWord(CharSequence word);
}
